
label ls_04_07_cornelia:
    scene black
    with Pause(1)

    show text "{color=#fff}{size=64}Cornelia Roberts{/size}{/color}" as title_text:
        xalign 0.5 yalign 0.4

    show text "{color=#fff}{size=30}Age: 43{/size}{/color}" as subtitle_text1:
        xalign 0.5 yalign 0.5
    with dissolve
    with Pause(2)

    show text "{color=#fff}{size=30}Tuesday, April 7{/size}{/color}" as subtitle_text2:
        xalign 0.5 yalign 0.75
    show text "{color=#fff}{size=30}07:00{/size}{/color}" as subtitle_text3:
        xalign 0.5 yalign 0.80
    with dissolve

    with Pause(2)

    hide title_text
    hide subtitle_text1
    hide subtitle_text2
    hide subtitle_text3
    with dissolve
    with Pause(1)

    "An alarm blared from a table next to my bed. I rolled over and slapped it until it turned off."

    "Once the noise stopped I questioned why I had an alarm {i}clock{/i} in the first place. Normally I just set a phone alarm."

    "Plus my chest felt way... heavier."

    scene bg leona room day with dissolve

    "Opening my eyes, I peered around a completely unfamiliar room."

    think "Where am..."

    think "Oh no."

    outfit cornelia nude

    show cornelia a_6 at centerright with dissolve

    "I hopped out of bed and sprinted to the nearest mirror where my greatest fear stared right back at me."

    cornelia "I'm Ms. Winters!?" with vpunch

    cornelia "Touching the rock worked!?" with vpunch

    cornelia "Arrrrg! Shit!!!" with vpunch

    placeholder


label ls_04_07_tori:
    scene black
    with Pause(1)

    show text "{color=#fff}{size=64}Tori Vega{/size}{/color}" as title_text:
        xalign 0.5 yalign 0.4

    show text "{color=#fff}{size=30}Age: 19{/size}{/color}" as subtitle_text1:
        xalign 0.5 yalign 0.5
    with dissolve
    with Pause(2)

    show text "{color=#fff}{size=30}Tuesday, April 7{/size}{/color}" as subtitle_text2:
        xalign 0.5 yalign 0.75
    show text "{color=#fff}{size=30}08:10{/size}{/color}" as subtitle_text3:
        xalign 0.5 yalign 0.80
    with dissolve

    placeholder

    with Pause(2)

    hide title_text
    hide subtitle_text1
    hide subtitle_text2
    hide subtitle_text3
    with dissolve
    with Pause(1)

    scene bg forest
    show tori a_0 at centerright, faceleft
    show vanessa a_0 at centerleft
    with dissolve

    "Tori and Vanessa go out into the woods for a smoke break."

    "Yui kept catching them on the school grounds so they figured if they went a deep enough nobody would know."

    "They light up only to see the strange light. They investigate."

    "Tori touches magic rock and gets electrocuted. Vanessa tries to pull her off and the energy transfers to her as well."

    body tori sandra suit

    scene bg classroom 1
    show tori a_0 at centerright, faceleft
    show riley a_7 at left

    "Tori is suddenly teaching Sandra's class."

    riley "-ifference between demo and kedo, again?"

    tori "...What?"

    "I looked down to see I was in a suit and heels."

    show tori b_2
    show riley a_4
    tori "...The fuck?"

    "The class gasped for some reason."

    riley "A-Are you feeling ok, Ms. Davis?"

    pause 1.0

    show tori b_5
    tori "I need another smoke."

label ls_04_07_jack:

    scene black
    with Pause(1)

    show text "{color=#fff}{size=64}Jack Mallory{/size}{/color}" as title_text:
        xalign 0.5 yalign 0.4

    show text "{color=#fff}{size=30}Age: 39{/size}{/color}" as subtitle_text1:
        xalign 0.5 yalign 0.5
    with dissolve
    with Pause(2)

    show text "{color=#fff}{size=30}Tuesday, April 7{/size}{/color}" as subtitle_text2:
        xalign 0.5 yalign 0.75
    show text "{color=#fff}{size=30}08:12{/size}{/color}" as subtitle_text3:
        xalign 0.5 yalign 0.80
    with dissolve

    with Pause(2)

    hide title_text
    hide subtitle_text1
    hide subtitle_text2
    hide subtitle_text3
    with dissolve
    with Pause(1)

    scene bg classroom 2
    show jack a_0 at centerright, faceleft

    "Jack is teaching class, humanize him at all."

    "An announcement over the intercom says to stay out of the woods because there's a magic rock."

    yui "{size=-5}Is this a prank?{/size}"

    abby "{size=-5}Please continue, Ms. Yamashita.{/size}"

    "Also Abby and Sayaka switched bodies so please address Abby as Sayaka and visa versa."

    "The class laughed."

    think "Strange. Why would Abby agree to something like this?"

    "Suddenly there's a flash and he passes out."

    body sandra tori uniform
    body jack vanessa uniform
    body vanessa jack uniform

    scene bg forest
    show sandra a_10 at centerright, faceleft
    show jack a_5 at centerleft
    with dissolve

    "When he wakes up he's in the middle of the woods with {q}Tori{/q}."

    jack "...I see you found a new smoking spot, Ms. Vega."

    "Eventually realizes that's actually Sandra."

    "Makes a bad joke about their predicament, which Sandra brushes off."

    "Sandra makes a joke about how he's in a much better position to bully John now. Jack kinda grimaces."

    "They head back to the school."

    "Abby is in the office in uniform."

    "Sandra and Jack are like... well I guess it's not a prank is it."

    "Abby requests Tori and Vanessa over the intercom."

    "Tori and Vanessa enter as well."

    "Run through what they're planning on doing."

    "Abby offers to cancel Sandra and Jack's classes."

    "Sandra says she can make it work. Jack admits he's on a pretty tight schedule and doesn't want to deviate."

    "...but they certainly can't teach dressed the way they are."

    outfit sandra uniform_full
    show sandra b_0

    "Sandra finds spare, non-ripped clothing."

label ls_04_07_sandra:

    scene black
    with Pause(1)

    show text "{color=#fff}{size=64}Sandra Davis{/size}{/color}" as title_text:
        xalign 0.5 yalign 0.4

    show text "{color=#fff}{size=30}Age: 19{/size}{/color}" as subtitle_text1:
        xalign 0.5 yalign 0.5
    with dissolve
    with Pause(2)

    show text "{color=#fff}{size=30}Tuesday, April 7{/size}{/color}" as subtitle_text2:
        xalign 0.5 yalign 0.75
    show text "{color=#fff}{size=30}17:30{/size}{/color}" as subtitle_text3:
        xalign 0.5 yalign 0.80
    with dissolve

    with Pause(2)
    outfit john uniform
    body sandra lsr_tori sandra
    scene bg main kitchen day
    show sandra a_1:
        offscreenright
        faceleft
        ease 1.0 center
    show john a_0:
        offscreenright
        faceleft
        ease 1.0 centerright
    show holly a_0:
        centerleft

    with dissolve


    $ sandra.name = "Sandra Davis"

    "After setting my bags down on taking off {q}Tori{/q}'s shoes John and I entered the kitchen to an anxious Holly."

    sandra "Hi, honey!"

    "Holly took a moment before she could bring herself to respond."

    holly "Well, either you've got an impeccable {q}Mom{/q} impression or you really are telling the truth."

    holly "She is telling the truth, right John? She didn't bribe you or anything, did you?"

    john "It's true. A couple other teachers also got switched with students including Principle Luten, and I've never seen her even entertain the thought of a prank."

    john "Plus Mom would have to pay me a {i}a lot{/i} to get me to play along with something this... bizarre."

    show sandra a_2
    show holly a_3
    show john a_5
    sandra "Tori and Mister Vega are going to be over for dinner soon to check in. If you don't believe me now I'm sure seeing Tori running around in my body will clear things up. {size=-5}I hope she wears something decent...{/size}"


label ls_04_07_vanessa:

    scene black
    with Pause(1)

    show text "{color=#fff}{size=64}Vanessa Russell{/size}{/color}" as title_text:
        xalign 0.5 yalign 0.4

    show text "{color=#fff}{size=30}Age: 39{/size}{/color}" as subtitle_text1:
        xalign 0.5 yalign 0.5
    with dissolve
    with Pause(2)

    show text "{color=#fff}{size=30}Tuesday, April 7{/size}{/color}" as subtitle_text2:
        xalign 0.5 yalign 0.75
    show text "{color=#fff}{size=30}21:30{/size}{/color}" as subtitle_text3:
        xalign 0.5 yalign 0.80
    with dissolve

    with Pause(2)

    "Vanessa in her room. Abby has come by to explain the situation to her parents."

    "She examines her body a bit."

    "Texts Tori and they talk about how weird everything is."

    "Mom comes in and says that she and dad will be as supportive as they can."

    placeholder