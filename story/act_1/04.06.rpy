label ls_04_06_abby:

    $ sandra.name = "Sandra Davis"
    $ lsr_aotsuka.name = 'Mrs. Sato'


    scene black
    with Pause(1)

    show text "{color=#fff}{size=64}Abby Luten{/size}{/color}" as title_text:
        xalign 0.5 yalign 0.4

    show text "{color=#fff}{size=30}Age: 54{/size}{/color}" as subtitle_text1:
        xalign 0.5 yalign 0.5
    with dissolve
    with Pause(2)

    # 2015
    show text "{color=#fff}{size=30}Monday, April 6{/size}{/color}" as subtitle_text2:
        xalign 0.5 yalign 0.75
    show text "{color=#fff}{size=30}16:15{/size}{/color}" as subtitle_text3:
        xalign 0.5 yalign 0.80
    with dissolve

    with Pause(2)

    hide title_text
    hide subtitle_text1
    hide subtitle_text2
    hide subtitle_text3
    with dissolve
    with Pause(1)

    think "Well, here we are: the start of yet another school year."

    think "Fresh youths joining the student body in while others return for another year."

    think "The biggest worries they have are {q}test scores{/q} and {q}who likes whom{/q}. What innocence. They have no idea how good they have it."

    think "And they won't know how good it really was until it's already gone."

    scene bg principal office day

    show abby a_0 at centerright, faceleft

    with dissolve

    "The back to school season always came with its own round of headaches. Inaccurate information in our computer system. Scheduling nightmares. {q}Last minute{/q} attendance submissions."

    "Those parents could have submitted those forms any damn time they pleased but they always had a knack for piling them up right before school started, saddling myself and Donna with a pile of work."

    "I stood and performed a couple basic wrist and back stretches to ease the throbbing of my muscles. My lower back and shoulders always seems to pop in a few more places than they did last."

    "Noting the time it looked like I was due for another round of pain pills, which I swallowed with a quick swig of afternoon coffee."

    "There was a knock at my door."

    abby "Come in."

    show donna a_3:
        offscreenleft
        ease 1.0 centerleft

    # I don't know things about school paperwork forgive me.
    donna "Good afternoon Ms. Luten, did you finish reviewing your half of the forms?"

    show donna a_6
    donna "We didn't get {i}too many{/i} new requests in today so I think we're finally slowing down."

    "I nodded toward the pile of my desk."

    abby "Of course. All finished."

    donna "Wonderful!"

    show donna a_3:
        centerleft
        ease 1.0 center
        faceleft
        pause .5
        ease 1.0 left
    "She scooped the paperwork off my desk in both her arms and carried it toward her own desk in the reception area."

    donna "Oh, sorry about this, but it sounds like there's {i}one more thing{/i} we'll want to look at today."

    "I sighed."

    abby "I'm sure. What is it?"

    show donna:
        faceright
    donna "Well, it's about clearing the woods for the new stadium: the workers found something."

    "I raised an eyebrow."

    abby "And there's a problem?"

    donna "I mean, they all ran off before the end of their shift, so I'd imagine so. Said something about a {q}cursed artifact way above our pay grade{/q}."

    donna "Though it's strange, the foreman couldn't remember anything about our agreement why I tried to discuss it with him."

    abby "That is strange. And annoying."

    "I stepped towards the door."

    show abby:
        centerright
        ease 1.0 centerleft

    abby "Well I suppose I'll go look at it. We'll need pictures when we sue the company for abandoning the project because of some baseless superstition."

    outfit sayaka cheer
    outfit cornelia cheer

    scene bg school exterior day
    show sayaka a_0 at center, faceleft
    show cornelia b_0 at centerleft
    with dissolve

    show abby a_0:
        faceleft
        offscreenright
        ease 1.0 right

    "The forest on the edge of our property was the last piece of land that we already owned, and could therefore cheaply expand into."

    "Exiting the administrative building I cut through the school grounds to get there, passing cheer practice on the way."

    show abby a_0:
        faceleft
        right
        ease 1.0 centerright

    show sayaka:
        faceright

    sayaka "Good afternoon, Ms. Luten! Come to see our new routine?"

    "Sayaka Sato: Model student, head cheerleader, and beautiful queen bee of the school. There were kids without aspirations here but she certainly wasn't one of them."

    "She did her best to hide her venomous side from upper faculty but I could see right through her. I played the crowd just like her when I was her age, after all."

    abby "Oh no, I'm afraid I'm on some official school business. Apparently there's been some hubub about that project in the woods so I thought I'd take a look."

    sayaka "Okay, well if you ever {i}did{/i} want to see just say the word."

    scene bg forest
    show abby a_0:
        faceleft
        right
        ease 1.0 centerright
    with dissolve

    "Despite the dense tree coverage obscuring a good portion of sunlight I noticed that my surrounding seemed to get brighter as I approached the construction site where those contractors were {i}supposed{/i} to be working."

    "Reaching the it I found a partially dug out hole with a strange looking stone placed in the center."

    "It had a slight metallic tinge to it and pulsed with a strange light of sorts, highlighting what appeared to be inscriptions scrawled across the entire surface."

    think "What a bunch of superstitious cowards. It's probably just a rare mineral that plays with sunlight in some novel way."

    think "I wonder if this writing is even English..."

    "I approached the stone and placed my hand on it with the intent of brushing the remaining dirt off."

    show white as flash:
        additive_flash(0.5)
    abby "{b}Ah!{/b}"

    show white as flash:
        additive_flash(0.5)
    "When I made contact an intense white light concentrated on the center of my hand and surged up and across my body."

    show white as flash:
        additive_flash(0.5)
    "Jolts electricity shot up my arm and locked my muscles in place."

    show white as flash:
        additive_flash(0.5)
    "I never thought {q}electrocution by abandoned stone{/q} was how I'd die but it looked like that was what was happening."

    hide flash
    show abbyGhost a_0:
        faceleft
        right
    with dissolve

    "The light continued to crawl up and across my body. It almost felt as if I were a third party observer, though I knew that had to just be my mind playing tricks."

    "My perspective shot out and away from my body through the woods and towards the school."

    show black with dissolve

    "I just held on to what I could, knowing I would meet my end at any moment."

    "At least I couldn't feel my aches and pains anymore."

    scene bg school exterior day
    show sayaka a_0 at centerright
    show cornelia b_0 at center
    with dissolve

    # collide with Sayaka

    show abbyGhost a_0:
        offscreenleft
        faceright
        ease .5 centerright

    "Finally I ran into something. I could only assume my legs had given out and I'd collapsed."

    possess abby sayaka a_0
    show abby at anim_fall_over_left

    "I hit the ground hard."

    show black

    body sayaka abby suit

    cornelia "-aka?"

    think "Hmm?"

    cornelia "-Ayaka?"

    think "That's strange... am I not dead?"

    cornelia "Sayaka!"

    scene bg school exterior day

    show cornelia b_0 at center
    with dissolve

    "I found myself lying prone on the school grounds, Cornelia Roberts staring at me with a look of deep concern."

    cornelia "Oh thank god! You're ok!"

    cornelia "Sadie ran to get the nurse. She'll be back any minute!"

    abby "Thank you very much, Ms. Roberts."

    "As I spoke the whole experience sounded... off. It was like someone had stretched out parts of my throat and squished others. Plus my voice sounded... airier."

    "Stranger still, all the aches and pains I'd grown so accustomed to over the years were... absent."

    think "Did the shock kill a bunch of nerve endings or something? Plus how did I even get here in the first place?"

    show abby a_0:
        centerright

    "I decided to get back on my feet with Ms Roberts' help."

    "She grasped my arm and pulled me back up to my feet. As she did there were subtle things here and there that seemed... off."

    "Standing felt effortless, yet there was also more weight to support on my chest, which was in a... cheerleading uniform?"

    "Looking down I saw that I was indeed dressed like a cheerleader."

    "Bringing a hand up to my face all the veins and wrinkles were absent, replaced with smooth white skin and some very well maintained nails."

    think "Maybe I really did die. It's as if I'm suddenly decades younger..."

    show grace a_0:
        offscreenright
        ease 1.0 right

    grace "Is everything ok, Sayaka? I heard you collapsed!"

    "I looked behind me to make sure she wasn't talking to someone behind me."

    abby "Did you just call me... Sayaka?"

    grace "Yes dear, that's your name. Did you forget it? Are you drinking enough water?"

    "I looked down at my body again, a few locks of pink hair fell in front of my face. Now that she said it out loud everything finally clicked."

    think "Oh my word. I've become Sayaka Sato!"

    "I couldn't believe my good luck."

    think "That rock must have killed me and sent me to heaven!"

    "I immediately collected myself. There was no way I'd botch an opportunity like this."

    abby "Don't worry, Ms. Reinhardt. I think I just got a little carried away. Now that I've sat for a minute I've feeling much better, now!"

    think "Worlds better."

    grace "Hmm, ok. Well I want you to take the next couple days off and drink plenty of water, ok?"

    abby "Oh, of course! Hehe~"

    sayaka "{size=+10}{b}Who... the fuck... are you!?{/b}{/size}"

    show sayaka a_0:
        offscreenleft
        ease 1.0 centerleft

    "A panting and wheezing {q}Abby Luten{/q} practically sprinted over to me before needing to take a knee to recover."

    sayaka "Hah... hahh... what the hell is going on? Why is... my body..?"

    grace "M-Ms. Luten, are you ok?"

    sayaka "I'm not..! Hold on..."

    show grace:
        right
        ease 1.0 left
        faceright

    "This other me good a few minute to catch her breath before Grace helped her to her feet."

    grace "I know I told you to get some exercise Ms. Luten but I didn't mean-"

    "At mention of my name she pushed Grace away."

    sayaka "{b}I'm not Ms. Luten! I'm Sayaka!{/b}"

    think "Oh."

    "Grace blinked in surprise."

    grace "But Sayaka's right here."

    sayaka "That must be a fake! I'm the real Sayaka!"

    cornelia "What are you talking about, Ms. Luten?"

    abby "Yeah, I think you must have hit your head or something."

    show sayaka:
        faceright
    "Sayaka grabbed Ms. Roberts by the shoulders and crouched to eye level."

    sayaka "Corny, it's me! I somehow got put in the body of Ms. Luten!"

    "Ms. Roberts did not seem convinced."

    cornelia "I'm sorry, but that's not possible, Ms-"

    sayaka "Yesterday we went to {q}Sweet Sixteen{/q}, {q}Thirty under Thirty{/q}, and {q}Kendo's{/q} and I got two new tops and you found a cute dress but we thought you looked fat in it so instead you got that striped dress and I thought it was so cute I got one that matched and we were going to coordinate on what days to wear it and-"

    "As Sayaka continued to recount as much information as she possibly could Ms. Roberts' face grew more and more confused. her eyes flitted between myself and Sayaka."

    cornelia "Did... what... is this a prank?"

    abby "Y-Yes, when you asked how my day went yesterday I didn't think it was going to be for this..."

    sayaka "If you're the real Sayaka, when's Corny's birthday?"

    abby "Oh, well that's easy. In fact I feel insulted that you even asked."

    "Cornelia laughed."

    cornelia "Yeah, seriously."

    pause 1

    "Sayaka raised an eyebrow."

    sayaka "Well if it's so obvious then you'd be willing to share it, right?"

    abby "So you can try to convince other people that you're really me?"

    pause 1

    cornelia "Seriously though, what's my birthday?"

    pause 1

    abby "June."

    pause 1

    abby "Eighth."

    sayaka "November Twelve. I got you that phone case last year."

    cornelia "Holy shit what happened, Sayaka!?"

    sayaka "So you believe me!?"

    cornelia "Of course!"

    "I let out a heavy sigh."

    abby "Well so much for that. I was hoping I'd died and reincarnated or something but it appears that Ms. Sato and really did switch bodies."

    grace "Wait, so this {i}wasn't{/i} some elaborate prank?"

    abby "Indeed. I'm really Abby Luten and that's really Sayaka Sato."

    grace "But how!?"

    abby "Well..."

    "I recounted how I had come across that strange stone, how it thrust me out of my body and into Ms. Sato's."

    grace "I need a drink..."

    sayaka "So if touching that rock switched you with me, maybe if I touch it we'll switch back?"

    "I gave out a long sigh."

    abby "Yes, I suppose it's worth a try."

    scene bg forest
    show abby a_0 at centerright, faceleft
    show sayaka a_0 at center
    show cornelia b_0 at left, faceright
    with dissolve

    "Grace told us she'd go see if she could find any other cases of {i}body switching{/i} and headed back to her office."

    "Meanwhile back in the clearing we approached the rock and put our palms on it. Cornelia maintained her distance."

    think "Goodbye, youth..."

    pause 1.0

    "The stone felt cold under my palm."

    cornelia "Did it work?"

    sayaka "I don't think so, not yet."

    pause 1.0

    sayaka "How long did it take you the first time?"

    pause 1.0

    abby "The moment I touched it I got shocked."

    extend " For whatever reason it's not working anymore."

    sayaka "Maybe it has to recharge or something!?"

    abby "Maybe, I'm not entirely sure how anything could have done this in the first place. Plus it doesn't seem like it's glowing anymore."

    cornelia "Seems pretty glowy to me."

    pause 1.0

    sayaka "We're stuck like this, aren't we?"

    abby "For the time being, it seems. The stone certainly isn't going anywhere so at least we have that assurance."

    "Sayaka let out a frustrated sigh and examined the stone, brushing off the dirt that had started this whole thing."

    sayaka "What is this? It doesn't look like English or Japanese..."

    "I peered closer myself."

    abby "No idea... never seen anything like it before."

    # It's Ainu writing

    "Sayaka took my phone out and motioned to snap a couple pictures only to realize there was no camera."

    sayaka "How old is this thing?"

    abby "It makes calls, that's all I need it for."

    sayaka "Well our phones are in our lockers so I guess we'll need to go grab that."

    scene bg lockerroom day
    show abby a_0 at centerright, faceleft
    show sayaka a_0 at center
    show cornelia b_0 at centerleft, faceright
    with dissolve

    "In the locker room Sayaka unlocked her locker and grabbed her purse with phone inside."

    abby "I'll go shower and change while you snap your photos."

    sayaka "Fine I guess. Just don't take too long. Wanna come with, Corny?"

    cornelia "Sure."

    outfit abby uniform

    scene bg school exterior dusk
    show abby a_0 at center, faceleft
    show sayaka a_0 at centerleft
    show cornelia b_0 at left, faceright
    with dissolve

    "One quick rinse and change later I was in a proper Tina Koya uniform."

    sayaka "Well we took a couple pictures, I'll try searching online tonight to see if I can find any leads."

    cornelia "Me too, though I gotta get going. Good luck with uh... this weird swap thing."

    abby "Excellent idea, let's {i}all{/i} retire for the evening. Would you like my address and keys, {q}Ms. Luten{/q}?"

    pause 1.0

    sayaka "Why did you call me that?"

    pause 1.0

    abby "Well I mean you can't go to the Sato household looking like me, can you? How would your parents react?"

    sayaka "I've gone through enough weird crap today. I'm not going to pretend that I'm you on top of everything else, Ms. Luten."

    sayaka "I'm going to go home, take a bath in my own bathroom, and sleep in my own bed."

    think "Why must you make things so difficult, Ms. Sato?"

    abby "Oh, if you insist..."

    scene bg abby car
    with fade

    "After collecting my things I offered to drive us to the Sato residence to explain our predicament."

    "However, sitting in the driver's seat gave me pause."

    abby "You wouldn't happen to have a driver's license, would you?"

    sayaka "I got one when I turned eighteen, why?"

    abby "Well, if I were to get pulled over while driving as you, I don't think they'd accept a story about switching bodies as an excuse for having what looks like a much older woman's id."

    abby "Would you mind trading?"

    sayaka "Yeah, I guess that makes sense... {size=5}and after all that effort to make sure I looked good in the photo...{/size}"

    "Sayaka pulled her id out of her purse and handing it over. I reciprocated."

    "I held the ID in my hands and looked it over."

    "{q}Name: Sayaka Sato{/q}"

    extend "\n{q}Gender: Female{/q}"

    extend "\n{q}Birthday: September 17th, 1996{/q}"

    "I read that line over and over again."

    think "That's right. My {i}new{/i} birthday. Not even two decades away."

    pause 1.0

    sayaka "I mean, I don't think my photo was {i}that good{/i}. {size=-5}You're practically drooling over it...{/size}"

    abby "Oh, sorry. I lost myself a bit there. It happens with age."

    sayaka "Uh huh. Can we get going?"

    abby "Of course, of course."

    # Sayaka residence

    scene bg sayaka kitchen dusk
    show abby a_0 at centerleft
    show sayaka a_0 at center
    show lsr_aotsuka a_0 at centerright
    with fade

    "Mrs. Sato welcomed us into the kitchen and started some tea."

    lsr_aotsuka "I certainly wasn't expecting you in person at this hour Ms. Luten, did something serious happen?"

    pause 1.0

    abby "She's talking to you."

    sayaka "Oh."

    sayaka "Well uh, you see... I'm not Ms. Luten. That's Ms. Luten."

    "She extended a finger towards me."

    sayaka "And I'm really Sayaka."

    lsr_aotsuka "...Excuse me?"

    abby "Hard to believe I'm sure, but your daughter-"

    sayaka "Step-daughter."

    abby "Your... {i}step{/i}-daughter is telling the truth. It appears that we switched bodies."

    "Mrs. Sato seemed taken aback, but not convinced."

    lsr_aotsuka "...Is this a joke?"

    lsr_aotsuka "{q}Body switching?{/q} You really expect me to believe something like that?"

    sayaka "Yup, that's really what happened. Ask me something only I would know."

    lsr_aotsuka "Well, um... what's your..."

    extend "favorite... color?"

    sayaka "Man you really suck sometimes Christy, I dunno what Dad saw in you."

    sayaka "I mean, other than your looks."

    lsr_aotsuka "Why you catty little-"

    "She stopped herself."

    lsr_aotsuka "Well, that certainly was a good impression but it still seems pretty far fetched to believe tha-"

    sayaka "I hid your earrings behind the sugar jar."

    pause 1.0

    lsr_aotsuka "What about my necklace?"

    sayaka "Let's not get greedy Christy."

    lsr_aotsuka "I'll buy you that new uPhone."

    abby "Erm."

    sayaka "Deal, but phone first."

    lsr_aotsuka "{i}Fiiiiiine.{/i} {size=-5}Bitch{/size}. We can go get it tomorrow."

    abby "Excuse me, ladies?"

    lsr_aotsuka "Oh, right."

    pause 1.0

    lsr_aotsuka "So like what are you going to do about school and stuff?"

    abby "Well the choice that would cause the least uproar would be to have your daughter-"

    lsr_aotsuka "Step-daughter."

    abby "Have your {i}step-daughter{/i} and I pretend to switch places while we try to reverse this."

    sayaka "Nope, not doing that."

    lsr_aotsuka "Wouldn't it be kinda embarrassing now that you're all {i}old{/i}?"

    "I cleared my throat."

    lsr_aotsuka "No offense."

    lsr_aotsuka "Like what would your um... orange haired friend say?"

    sayaka "Corny. And I'll live."

    lsr_aotsuka "Your dad isn't going to be back for another week, should we let him know now or...?"

    sayaka "Yeah, how about you tell him? Even if he doesn't believe you at least it won't come as a shock or anything."

    abby "In the meantime I'd like to borrow some more fitting clothes from your daughter. She's welcome to do the same with my own wardrobe."

    sayaka "Thanks for offering but I think I'd rather go buy my own stuff. I have a feeling our styles don't really like up."

    lsr_aotsuka "I mean it looks like we're the same size now so why don't we share?"

    "Sayaka blanched."

    sayaka "I'd rather eat a bowl of nails."

    lsr_aotsuka "Yeah yeah, I'll give it a day before you come crying to me over how I keep myself looking young and need to borrow a cute top."

    pause 1.0

    sayaka "You can still borrow some of the stuff I guess, just check with me first. I have a couple things I rather save for when I'm back in my body."

    abby "Fine with me."

    scene bg abby livingroom evening

    show abby a_0 at center

    with fade

    "After a grueling process where Sayaka outlined what clothes I could take and what I was allowed to match with what I was back home with a basket of clothes befitting the teenage girl I had become."

    "Closing and locking the door behind me I set my things down."

    pause 1.0

    abby "He he he he..."

    pause 1.0

    abby "Ha ha ha ha ha!"

    abby "{b}Hahahahahahahahahahaha!{/b}"

    "I clutched my sides and laughed harder than I had in years."

    "There wasn't anything particularly funny about the situation, but the sheer amount of joy had been repressing all afternoon finally welled up to the top."

    "I practically skipped up to the bathroom, humming all the way."

    # Abby goes home
    scene bg abby bath full
    show abby a_0 at center
    with dissolve

    outfit abby nude
    with dissolve
    "After starting a bath-- my reward for keeping my cool for the afternoon-- I disrobed."

    "I stood in front of the mirror and gave myself a moment to take the entirety of my {i}{b}new{/b} body{/i} in."

    "All my wrinkles, greying hairs, ugly veins, they were all gone. Replaced with smooth beautiful skin."

    "No split ends, nothing out of place. Just the perfect feminine ideal. And it was all {i}mine{/i}."

    "I cupped my breasts, feeling how perky and spritely they were under my palms."

    think "Practically perfect in every way."

    "I gingerly dipped myself into the tub, feeling the warm water well up around me. Instead of easing my aches and pains to a more tolerable level the water relaxed my muscles and tender skin."

    "Once I settled in I let my thoughts drift, though it didn't take long for my mind's current to take me to the question of the evening."

    think "How do I keep this is permanent?"

    think "Sayaka clearly has no interest in keeping this a secret, so the people in her social circles will be certainly be aware that a switch happened."

    think "Though I'm sure anyone outside of that will just thing we're playing a weird prank."

    think "Plus if it's just us two then eventually it'll just be written off as some freak accident. A couple news stories get published and then the public forgets."

    think "Meanwhile I comfort Sayaka, telling her that I'm {q}looking into it{/q} and just bide my time."

    pause 1.0

    think "Although..."

    extend " if the stone swaps a few more people, they're going to want figure out how to switch back."

    think "If it's just a handful I might be able to manage it, depending on who gets involved. I'm sure a few faculty members would be just as happy with the situation as me."

    think "If a large enough number of people get switched... that'll get the media and outside organizations much more involved. I need to prevent that at all costs."

    "I poured a bucket of warm water over my head to rinse the shampoo off. It felt heavenly as it ran down my back and shoulders."

    think "So the first order of business tomorrow will be to make sure nobody else gets anywhere near that rock. A stern announcement should ward off most of the student body."

    think "I'll also need to explain the swap in a way that's round about enough that people don't go intentionally starting their own."

    think "Next I'll need a more permanent means to block off access. Perhaps that construction company could {q}accidentally{/q} bury it for me."

    extend " Or smash it to bits."

    "Thoughts of the only conduit of returning to my body exploding into pieces filled my chest with a warm fuzzy feeling."

    think "Mmm, yes... this youthful body for the rest of my life..."

    think "Or even better: the company buries the rock and in another thirty years they dig it back up and I switch with {b}another{/b} student."

    "I submerged the lower half of my head under the water and blew bubbles out my nose."

    think "Mmmm, now wouldn't that be wonderful~?"

    scene bg laura bedroom night

    show abby a_0 at center

    with dissolve

    think "Hmm, now what {i}will{/i} I wear tomorrow?"

    think "I would have loved to wear a uniform again but with Sayaka holding on to her old life I suppose I'll have to engage in this {q}charade{/q} with her, won't I?"

    "I opened my closet where an array of nicely pressed suits were hung."

    think "Hmm, the fit is going to be a {i}tad{/i} off on these until I make a trip to the taylor. Plus there's no need to be entirely convincing about my identity, is there?"

    outfit abby formal
    with dissolve
    "Sifting through the clothes Sayaka could bear to part with found a modest blouse and skirt that fit my style quite nicely."

    "I swayed my body back and forth, taking in all the new sensations associated with being such a young beautiful girl."

    "I looked in the mirror and smiled."

    scene black
    with fade

    "Maybe this year wouldn't be so bad after all."

    show text "{color=#fff}{size=64}Sayaka Sato{/size}{/color}" as title_text:
        xalign 0.5 yalign 0.4

    show text "{color=#fff}{size=30}Age: 54{/size}{/color}" as subtitle_text1:
        xalign 0.5 yalign 0.5
    with dissolve
    with Pause(2)

    show text "{color=#fff}{size=30}Monday, April 6{/size}{/color}" as subtitle_text2:
        xalign 0.5 yalign 0.75
    show text "{color=#fff}{size=30}20:45{/size}{/color}" as subtitle_text3:
        xalign 0.5 yalign 0.80
    with dissolve

    with Pause(2)

    hide title_text
    hide subtitle_text1
    hide subtitle_text2
    hide subtitle_text3
    with dissolve
    with Pause(1)

    outfit cornelia casual

    scene bg school exterior night

    show sayaka a_0 at center

    with dissolve

    "I tapped my foot impatiently. It wasn't {i}quite{/i} the time that Corny and I agreed upon, but it was getting close."

    "Finally I saw her approach."

    show cornelia a_0:
        offscreenright
        ease 1.0 centerright
    cornelia "Hey Saya, how's the old hag body holding up?"

    sayaka "Terrible. I've got headaches and sore muscles and I swear I got winded just walking here."

    cornelia "You think Ms. Luten knows we're here?"

    sayaka "She's still texting me with questions about how to best wear my hair and what skin products to use so I don't think so."

    cornelia "Gross."

    sayaka "I'm telling you, she's a complete psycho! Half the time she was trying to take over my life and the other half was her drooling over having my body!"

    "Corny shivered."

    cornelia "Why'd it have to be her of all people?"

    scene bg forest

    show sayaka a_0 at center
    show cornelia a_0 at centerright

    with dissolve


    "We reached the woods. The stone wouldn't be much further."

    cornelia "You really think she's going to try smashing the rock or something?"

    sayaka "Nah, then she couldn't use it again once {i}my{/i} body gets old. She's probably going to try to like, bury it or wall it off or something."

    cornelia "Or {q}accidentally{/q} encase it in cement."

    sayaka "Exactly."

    "Corny slid down an incline and hopped a couple times to regain her footing. I took it very slowly and carefully. I couldn't risk falling and breaking something."

    cornelia "It should be close, I can see that glowing again."

    sayaka "What glowing?"

    "After turning past an outcropping of trees we found the stone still just as dull as I saw it last."

    cornelia "You really can't see it? Maybe Ms. Luten's eyes suck."

    cornelia "Or like how once you're old enough you stop hearing certain sounds but with weird glowey rocks."

    "I ignored her and reached out to touch the stone again. Nothing."

    cornelia "Feel any different?"

    sayaka "...No."

    "I brushed more of the dirt off the stone. The entire thing was covered head to toe in writing. Since it had only partially been unearthed the writing continued well past the ground."

    cornelia "Well that {q}ancient language interest group{/q} said they needed more samples at a {q}much higher quality{/q} so I guess we can start with that."

    "I continued brushing the stone off while Corny snapped photo after photo at as high a focus as she could handle."

    sayaka "Well here's hoping this top part is enough to at least give us {i}something{/i}."

    pause 1.0

    cornelia "So you ready to go home or..."

    pause 1.0

    sayaka "You haven't tried touching the stone yet, have you Corny?"

    pause 1.0

    sayaka "I wonder if it'd work on you..."

    cornelia "What!? Hell no, I'm not going anywhere near that thing! It looks radioactive or something!"

    sayaka "What are you talking about? It's super dull! I'm surprised you weren't using your phone's flash or something when you were snapping photos!"

    sayaka "I bet the power's off and it just won't do anything."

    cornelia "You're just saying that because you don't want to be old by yourself."

    pause 1.0

    sayaka "Corny, I... I need to know if the stone still works."

    sayaka "I just feel like Ms. Luten's going to just cover this whole thing up and convince the world that we never swapped and I'm going to get stuck as her and and and-"

    "My breathing grew faster and faster, yet for some reason I couldn't get enough air. I clutched my chest. Corny rushed to grab me."

    cornelia "But I believe you, Saya! And your step-mom does too! It's super obvious!"

    sayaka "Yeah but do you think like John Doe in class B-2 is going to care? What about the cops? The doctor? My therapist? Most people are going to just think I'm faking it until... until"

    sayaka "Until... I give up. And Ms. Luten takes over my life..."

    "Tears welled up as the enormity of the situation crashed around me. I contemplated whether I'd rather be dead."

    cornelia "I'm not going to let that happen, Saya! There's no way!"

    "Corny let go over and took a long hard look at the stone."

    cornelia "Fine. Fuck it. Let's find out if the stone still works."

    cornelia "If it does then we're in this together and we learn something about how it works. If it doesn't then... well, we're {i}still{/i} in this together."

    "Corny reached a quivering hand out toward the stone and closed her eyes."

    "When she made contact her body immediately convulsed."

    cornelia "{b}Ahhhhhhhh!{/b}"

    sayaka "C-Corny!"

    "I rushed to her and tried to pull her away but her body wouldn't budge no matter how hard I pulled."

    show corneliaGhost a_0:
        right
        ease 1.0 offscreenright

    "Light flashed throughout her body before finally a large surge of it collected and shot out. She went limp."

    sayaka "Oh-my-god-I-just-killed-my-best-friend!"

    show leonaGhost a_0:
        offscreenright
        ease 1.0 right

    possess leona cornelia a_0
    "But before I could mourn another flash of light shot back into her body and life returned to her."

    sayaka "Corny! Corny!"

    body cornelia leona casual
    show leona a_0:
        centerright

    $ leona.name = "???"

    leona "Muhh..."

    sayaka "Corny, are you ok!?"

    leona "Ms. Luten, what are you doing out here?"

    leona "Why are we..."

    "{q}Corny{/q} looked down at herself and back up at me."

    leona "I'm so small! Hehe!"

    sayaka "Who {i}are{/i} you!?"

    leona "Gwendolyn Whibnablemslm..."

    sayaka "Excuse me?"

    leona "I'm g... g..."

    "{q}Corny{/q} blinked as few times and looked around."

    leona "Hmm. How'd I get out here?"

    leona "For a dream this is strangely realistic. I can feel and hear everything."

    sayaka "{b}Who are you!?{/b}"

    $ leona.name = "Leona Winters"

    leona "Huh? I'm Leona. You should know that already, Abby."

    "I let go of her and dropped to my knees."

    sayaka "Oh shit. It worked."

    leona "What worked?"

    sayaka "Corny touched that rock over there and now you're in her body and she's in yours."

    leona "I'm in-"

    "Ms. Winters truely looked down at herself for the first time."

    leona "Oh my lord. Why am I in the body of Ms. Roberts, Abby!?"

    sayaka "I'm not Abby. I'm Sayaka. The rock switched me too."

    leona "{q}Rock?{/q} What are you talking about?"

    sayaka "That glowing stone. Corny touched it and it switched you."

    leona "Seems rather dull to me."

    sayaka "What?"

    leona "The stone. It has a slight sheen but it certainly isn't glowing or anything."

    sayaka "What happens if you touch it?"

    "Ms. Winters put her palm on it."

    leona "It's cold, I suppose."

    sayaka "Well I guess that explains that part. It only glows if it can switch you."

    leona "Could we please get out of here? It's getting a tad chilly and I'd really like to go to bed so I can wake up in my own body."

    sayaka "That's not- actually nevermind, sure. Let's go. Where do you live?"

    scene bg main house night

    show sayaka a_0:
        offscreenright
        ease 1.5 centerleft

    show leona a_0:
        offscreenright
        ease 1.5 left

    with dissolve

    "One short drive later we arrived at Ms. Winters' house. It was well past eleven thirty at this point so there weren't many people out."

    sayaka "Ugh, it's locked."

    leona "Do you not lock your front door?"

    sayaka "I mean I do, it's just annoying. Do you have a spare hidden somewhere?"

    leona "No need. Sandra has one, though she's likely not up at this hour either."

    sayaka "Great."

    "I knocked as loudly as I could." with vpunch

    sayaka "{b}Corny! Wake up!{/b}" with vpunch

    pause 1.0

    "Nothing."

    "I knocked again. A dog started barking across the street."

    sayaka "Shit. At this rate we're gonna have the cops called on us or something."

    leona "Perhaps we should go somewhere else. I'm sure sleeping any bed will do the trick to switch me back."

    sayaka "Yeah yeah, I guess let's go to my place."



    scene bg sayaka kitchen dusk
    show lsr_aotsuka a_0 at left
    show sayaka a_0:
        offscreenright
        ease 1.0 center
    show leona a_0:
        offscreenright
        ease 1.0 centerright
    with fade

    lsr_aotsuka "Back from looking at that rock?"

    sayaka "Yeah."

    lsr_aotsuka "Any leads? I see you brought your friend... uh... Carrot, was it?"

    leona "Leona."

    lsr_aotsuka "Oh! What class are you in?"

    leona "I teach English."

    pause 1.0

    lsr_aotsuka "Sayaka did you get your friend to touch the rock?"

    pause 1.0

    sayaka "Yes."

    pause 1.0

    show lsr_aotsuka:
        left
        ease 1.0 offscreenleft

    lsr_aotsuka "Well there's an extra futon in the closet if you need it, I'm going to bed."

    lsr_aotsuka "If you need a drink or something Leona there's some box wine in the fridge. Help yourself."

    leona "Okay, thank you Ms. Sato."

    scene black with dissolve

    "After laying out the futon and lending Ms. Winters some of my pajamas we laid down to go to sleep."

    leona "You know, even though this was a dream I don't think I'd mind spending a little more time like this."

    leona "I feel so small and nimble. It's just so different from my normal body."

    sayaka "Uh huh."

    pause 1.0

    sayaka "Well, good night Ms. Winters."

    leona "Good night, Ms. Luten."

    sayaka "Sayaka."

    leona "Right. Good night, Sayaka."

    jump ls_04_07_cornelia
